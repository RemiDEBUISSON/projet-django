from django.contrib import admin
from .models import Item, ItemHistory, SharedItem, User


# admin.site.register(User),
class ItemAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "user_id",
        "name",
        "password",
        "url",
        "created_date",
        "updated_date",
    )


class SharedItemAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "user_from_id",
        "user_to_id",
        "item_id",
        "created_date",
    )


class ItemHistoryAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "item_id",
        "name",
        "password",
        "url",
        "action",
        "updated_date",
    )


class UserAdmin(admin.ModelAdmin):
    list_display = ("id",)


admin.site.register(User, UserAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(ItemHistory, ItemHistoryAdmin)
admin.site.register(SharedItem, SharedItemAdmin)
