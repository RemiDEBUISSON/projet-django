from django import forms
from .models import Item, User, SharedItem
from django.contrib.auth.forms import UserCreationForm

import datetime


class ItemForm(forms.ModelForm):
    name = forms.CharField(
        max_length=50,
        widget=forms.TextInput(attrs={"class": "form-control input"}),
    )
    password = forms.CharField(
        widget=forms.TextInput(attrs={"class": "form-control input", "id": "password"}),
    )
    url = forms.CharField(
        max_length=255,
        widget=forms.TextInput(attrs={"class": "form-control input"}),
    )

    class Meta:
        model = Item
        fields = ["name", "password", "url"]

    def init(self, args, **kwargs):
        super(ItemForm, self).init(args, **kwargs)


class NewUserForm(UserCreationForm):
    username = forms.CharField(
        max_length=150,
        widget=forms.TextInput(attrs={"class": "form-control input"}),
    )
    email = forms.EmailField(
        required=False, widget=forms.EmailInput(attrs={"class": "form-control input"})
    )
    password1 = forms.CharField(
        max_length=255,
        widget=forms.TextInput(attrs={"class": "form-control input", "id": "password"}),
    )
    password2 = forms.CharField(
        max_length=255,
        widget=forms.TextInput(attrs={"class": "form-control input"}),
    )

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")

    def save(self, commit=True):
        user = super(NewUserForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user


class UpdateUserForm(forms.ModelForm):
    username = forms.CharField(
        max_length=150,
        widget=forms.TextInput(attrs={"class": "form-control input"}),
    )
    email = forms.EmailField(
        required=False, widget=forms.TextInput(attrs={"class": "form-control input"})
    )

    class Meta:
        model = User
        fields = ["username", "email"]


class SharedItemForm(forms.ModelForm):
    user_to_id = forms.ModelChoiceField(
        queryset=User.objects.all(), widget=forms.Select()
    )

    class Meta:
        model = SharedItem
        fields = ["user_to_id"]

    def init(self, args, **kwargs):
        super(SharedItemForm, self).init(args, **kwargs)
