from datetime import datetime

filename = "app/log.log"


def write_log(log_type, username, message_log):
    now = datetime.now()
    timestamp = now.strftime("%Y-%m-%d %H:%M:%S")
    log_entry = f"[{timestamp}] [{log_type.upper()}] [{username}]: {message_log}\n"

    with open(filename, "a") as file:
        file.write(log_entry)
