from django.shortcuts import render, redirect
from django.urls import get_resolver
from django.views.decorators.http import require_http_methods
from django.contrib.auth import login
from django.http import HttpResponse
from django.contrib import messages

from .models import Item, SharedItem, User, ItemHistory
from .forms import ItemForm, NewUserForm, UpdateUserForm, SharedItemForm
from .service.encryption_util import encrypt, decrypt
from .service.loging import *


def home(request):
    current_user = request.user
    write_log("info", current_user.username, "Page Home")
    return render(request, "home.html")


def items_list(request):
    current_user = request.user
    if current_user.id == None:
        return redirect("/accounts/login")
    write_log("info", current_user.username, "Page item list")

    items = Item.objects.all().filter(user_id=current_user.id)
    sharedItems = SharedItem.objects.all().filter(user_to_id=current_user.id)
    sharedItemsList = []
    for sharedItem in sharedItems:
        item = Item.objects.get(id=sharedItem.item_id.id)
        item.name = decrypt(item.name)
        item.url = decrypt(item.url)
        sharedItemsList.append(item)
    urls = get_resolver().url_patterns
    for item in items:
        item.name = decrypt(item.name)
        item.url = decrypt(item.url)
    return render(
        request,
        "items_list.html",
        context={
            "items": items,
            "sharedItems": sharedItemsList,
            "urls": urls,
            "user": current_user,
        },
    )


@require_http_methods(["GET", "POST"])
def update_item(request, number):
    current_user = request.user
    if current_user.id == None:
        return redirect("/accounts/login")
    error = ""

    item = Item.objects.get(id=number)
    if item.user_id != current_user:
        write_log("Error", current_user.username, "unauthorized access attempt")
        return redirect("items_list")

    print(1, item.name)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=item)
        print(2, form)
        if form.is_valid():
            item = Item.objects.get(id=number)
            print(3, item.name)

            if form.cleaned_data["password"] != item.password:
                print(4, form.cleaned_data)
                form.save()
                item = Item.objects.get(id=number)
                item.password = encrypt(item.password)
                item.url = encrypt(item.url)
                item.name = encrypt(item.name)
                item.save()

                itemhistory = ItemHistory.objects.create(
                    item_id=item,
                    name=item.name,
                    password=item.password,
                    url=item.url,
                    action="update Item",
                    updated_date=item.updated_date,
                )
                itemhistory.save()

                print(5, item.name)
                write_log("info", current_user.username, "Item updated")
                return redirect("items_list")

            else:
                error = "Please use a new password !"
                write_log(
                    "error",
                    current_user.username,
                    "Failed to update item, same password",
                )
                form = ItemForm(
                    data={
                        "password": item.password,
                        "name": item.name,
                        "url": item.url,
                    }
                )
                print(6, form.cleaned_data)
    else:
        form = ItemForm(
            data={
                "password": decrypt(item.password),
                "name": decrypt(item.name),
                "url": decrypt(item.url),
            }
        )
    write_log("info", current_user.username, "Page update list")
    return render(
        request,
        "update_item.html",
        context={
            "item": item,
            "user": current_user,
            "form": form,
            "error": error,
        },
    )


@require_http_methods(["GET", "POST"])
def create_item(request):
    current_user = request.user
    if current_user.id == None:
        return redirect("/accounts/login")

    form = ItemForm(data={"password": "", "name": "", "url": ""})

    if request.method == "POST":
        form = ItemForm(request.POST)

        if form.is_valid():
            name = request.POST.get("name")
            password = request.POST.get("password")
            url = request.POST.get("url")
            item_object = Item.objects.create(
                user_id=current_user,
                name=encrypt(name),
                password=encrypt(password),
                url=encrypt(url),
            )
            item_object.save()

            itemhistory = ItemHistory.objects.create(
                item_id=item_object,
                name=item_object.name,
                password=item_object.password,
                url=item_object.url,
                action="create Item",
                updated_date=item_object.updated_date,
            )
            itemhistory.save()

            write_log("info", current_user.username, "Item created in database")

            return redirect("/items_list")

        else:
            form = ItemForm(data={"password": password, "name": name, "url": url})
            write_log(
                "error", current_user.username, "Failed to create item in database"
            )
            return render(
                request,
                "create_item.html",
                context={
                    "name": name,
                    "password": password,
                    "url": url,
                    "form": form,
                },
            )

    write_log("info", current_user.username, "Page create item")
    return render(
        request,
        "create_item.html",
        context={
            "form": form,
        },
    )


def delete_item(request, number):
    current_user = request.user
    if current_user.id == None:
        return redirect("/accounts/login")

    item = Item.objects.get(id=number)
    if item.user_id == current_user:
        write_log("info", current_user.username, "Item dropped in database")
        item.delete()

    write_log("error", current_user.username, "Failed to drop item")
    return redirect("/items_list")


@require_http_methods(["GET", "POST"])
def register(request):
    current_user = request.user
    if current_user.id != None:
        return redirect("home")

    if request.method == "POST":
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            write_log("info", user.username, "New suer register")
            messages.success(request, "Registration successful.")
            return redirect("home")
        else:
            write_log("error", current_user.username, "Failed to register a new user")
            return render(
                request=request,
                template_name="registration/register.html",
                context={"register_form": form},
            )

    form = NewUserForm()
    write_log("error", current_user.username, "Page register user")
    return render(
        request=request,
        template_name="registration/register.html",
        context={"register_form": form},
    )


def update_profile(request):
    current_user = request.user
    if current_user.id == None:
        return redirect("home")

    if request.method == "POST":
        user_form = UpdateUserForm(request.POST, instance=request.user)

        if user_form.is_valid():
            try:
                user = User.objects.get(username=request.POST.get("username"))
                write_log("error", current_user.username, "Failed to update username")
                return render(
                    request,
                    "profile.html",
                    {
                        "user_form": user_form,
                        "error": "username already exists",
                    },
                )
            except:
                write_log("info", current_user.username, "Username updated")
                user_form.save()
                messages.success(request, "Your profile is updated successfully")
                return redirect(to="changedmymind")

        else:
            write_log(
                "error",
                current_user.username,
                "Failed to update username, invalide form",
            )
            return render(
                request,
                "profile.html",
                {"user_form": user_form},
            )

    else:
        user_form = UpdateUserForm(instance=request.user)

    write_log("info", current_user.username, "Page changemymind")
    return render(
        request,
        "profile.html",
        {"user_form": user_form},
    )


@require_http_methods(["GET", "POST"])
def share_item(request, number):
    current_user = request.user
    item = Item.objects.get(id=number)
    if current_user.id == None:
        return redirect("/accounts/login")

    if item.user_id != current_user:
        return redirect("items_list")

    if request.method == "POST":
        form = SharedItemForm(request.POST)
        print(request.POST.get("user_to_id"))
        user_to = User.objects.get(id=request.POST.get("user_to_id"))
        if form.is_valid():
            user_to_id = user_to
            user_from_id = current_user
            item_id = item
            sharedItem_object = SharedItem.objects.create(
                user_to_id=user_to_id, user_from_id=user_from_id, item_id=item_id
            )
            sharedItem_object.save()

            write_log("info", current_user.username, "item Shared")
            return redirect("/items_list/")

    form = SharedItemForm()
    write_log("info", current_user.username, "Page Share item")
    return render(
        request,
        "share_item.html",
        context={
            "item": item,
            "user": current_user,
            "form": form,
        },
    )


def shared_items(request):
    current_user = request.user
    if current_user.id == None:
        return redirect("/accounts/login")

    sharedItems = SharedItem.objects.all().filter(user_from_id=current_user.id)
    shared = []
    for sharedItem in sharedItems:
        user_to = User.objects.get(id=sharedItem.user_to_id.id)
        item = Item.objects.get(id=sharedItem.item_id.id)
        item.name = decrypt(item.name)
        item.url = decrypt(item.url)
        shared.append(
            {
                "item": item,
                "user": user_to,
                "share_created_date": sharedItem.created_date,
            }
        )

    urls = get_resolver().url_patterns
    write_log("info", current_user.username, "Page Shared item")
    return render(
        request,
        "shared_items.html",
        context={
            "shared": shared,
            "urls": urls,
            "user": current_user,
        },
    )


def history(request):
    current_user = request.user
    if current_user.id == None:
        return redirect("/accounts/login")

    history = ItemHistory.objects.all()
    historyChanges = []
    for historyChange in history:
        item = Item.objects.get(id=historyChange.item_id.id)
        print(item.name)
        item.name = decrypt(item.name)
        item.url = decrypt(item.url)
        # item.password = decrypt(item.password)
        historyChanges.append(
            {
                "item": item,
                "history_info": historyChange,
            }
        )

    urls = get_resolver().url_patterns
    write_log("info", current_user.username, "Page Items History")
    return render(
        request,
        "history.html",
        context={
            "historyChanges": historyChanges,
            "urls": urls,
            "user": current_user,
        },
    )


@require_http_methods(["POST"])
def password(request):
    if request.method == "POST":
        password = request.POST.get("password")
        decrypt_password = decrypt(password)
        return HttpResponse(decrypt_password)
