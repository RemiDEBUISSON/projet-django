from django.db import models
from django.db.models import Model
from django.contrib.auth.models import AbstractUser, User


# Create your models here.
class User(AbstractUser):
    pass
    # is_admin = models.BooleanField(default=False)


class Item(Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(blank=False, null=False, max_length=255)
    password = models.CharField(blank=False, null=False, max_length=255)
    url = models.CharField(blank=False, null=False, max_length=255)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)


class SharedItem(Model):
    user_from_id = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="shared_items_from"
    )
    user_to_id = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="shared_items_to"
    )
    item_id = models.ForeignKey("Item", on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)


class ItemHistory(Model):
    item_id = models.ForeignKey("Item", on_delete=models.CASCADE)
    name = models.CharField(blank=False, null=False, max_length=255)
    password = models.CharField(blank=False, null=False, max_length=255)
    url = models.CharField(blank=False, null=False, max_length=255)
    action = models.CharField(blank=False, null=False, max_length=255)
    updated_date = models.DateTimeField(auto_now=True)
