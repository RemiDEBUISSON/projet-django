from django.contrib import admin
from django.urls import path


from .views import *

urlpatterns = [
    path("items_list/", items_list, name="items_list"),
    path("update_item/<int:number>", update_item, name="update_item"),
    path("create_item/", create_item, name="create_item"),
    path("delete_item/<int:number>", delete_item, name="delete_item"),
    path("share_item/<int:number>", share_item, name="share_item"),
    path("", home, name="home"),
    path("register/", register, name="register"),
    path("changedmymind/", update_profile, name="changedmymind"),
    path("password/", password, name="password"),
    path("shared_items/", shared_items, name="shared_items"),
    path("history/", history, name="history"),
]
